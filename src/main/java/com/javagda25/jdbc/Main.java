package com.javagda25.jdbc;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        StudentDao studentDao;
        try {
            studentDao = new StudentDao();
        } catch (SQLException e) {
            System.err.println("Student dao cannot be created. Mysql error.");
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace();
            return;
        } catch (IOException e) {
            System.err.println("Configuration file error.");
            System.err.println("Error: " + e.getMessage());
            return;
        }

        Scanner scanner = new Scanner(System.in);

        String komenda;
        do {
            komenda = scanner.nextLine();
            try {
                if (komenda.equalsIgnoreCase("wstaw")) {
                    System.out.println("Imie:");
                    String imie = scanner.nextLine();
                    System.out.println("Wiek:");
                    int wiek = Integer.parseInt(scanner.nextLine());
                    System.out.println("Srednia:");
                    double srednia = Double.parseDouble(scanner.nextLine());
                    System.out.println("Czy zywy:");
                    boolean zywy = Boolean.parseBoolean(scanner.nextLine());

                    Student student = new Student(imie, wiek, srednia, zywy);
                    studentDao.insertStudent(student);
                } else if (komenda.equalsIgnoreCase("usun")) {
                    System.out.println("Podaj id:");
                    Long studentId = Long.parseLong(scanner.nextLine());

                    System.out.println(studentDao.deleteStudent(studentId));
                } else if (komenda.equalsIgnoreCase("list")) {
                    studentDao.listAllStudents().forEach(System.out::println);
                }
            } catch (SQLException e) {
                System.err.println("Error executing command: " + e.getMessage());
            }
        } while (!komenda.equalsIgnoreCase("quit"));
    }


}
