package com.javagda25.jdbc;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.javagda25.jdbc.StudentQueries.*;

public class StudentDao { // data access object
    private MysqlConnection mysqlConnection;

    public StudentDao() throws SQLException, IOException {
        mysqlConnection = new MysqlConnection();

        createTableIfNotExists();
    }

    private void createTableIfNotExists() throws SQLException {
        try (Connection connection = mysqlConnection.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(CREATE_TABLE_QUERY)) {
                statement.execute();
            }
        }
    }

    public void insertStudent(Student student) throws SQLException {
        try (Connection connection = mysqlConnection.getConnection()) {

            // jdbc-carshop
            try (PreparedStatement statement = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS)) {
                statement.setString(1, student.getName());
                statement.setInt(2, student.getAge());
                statement.setDouble(3, student.getAverage());
                statement.setBoolean(4, student.isAlive());

//              boolean areThereAnyResultData = statement.execute(); // jeśli interesuje nas czy w wyniku otrzymaliśmy dane
                int affectedRecords = statement.executeUpdate(); // jeśli interesuje nas ile rekordów zostało zmodyfikowanych

                ResultSet resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    Long generatedId = resultSet.getLong(1);
                    System.out.println("Został utworzony rekord o identyfikatorze: " + generatedId);
                }
            }
        }

    }

    public boolean deleteStudent(Long studentId) throws SQLException {
        try (Connection connection = mysqlConnection.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(DELETE_QUERY)) {
                statement.setLong(1, studentId);

                int affectedRecords = statement.executeUpdate();

                if(affectedRecords > 0){
                    // usunelismy rekord.
                    return true;
                }
            }
        }
        return false;
    }

    public Optional<Student> getByIdStudent(Long searchedId) throws SQLException {
        try (Connection connection = mysqlConnection.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(SELECT_BY_ID_QUERY)) {
                statement.setLong(1, searchedId);

                ResultSet resultSet = statement.executeQuery();

                if (resultSet.next()) { // jeśli jest rekord
                    Student student = loadStudentFromResultSet(resultSet);

                    return Optional.of(student);
                }
            }
        }
        return Optional.empty();
    }

    public List<Student> listAllStudents() throws SQLException {
        List<Student> studentList = new ArrayList<>();

        try (Connection connection = mysqlConnection.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(SELECT_ALL_QUERY)) {
                ResultSet resultSet = statement.executeQuery();

                loadMultipleStudentsFromResultSet(studentList, resultSet);
            }
        }
        return studentList;
    }

    public List<Student> listAllWithAgeBetween(int from, int to) throws SQLException {
        List<Student> studentList = new ArrayList<>();

        try (Connection connection = mysqlConnection.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(SELECT_ALL_QUERY)) {
                statement.setInt(1, from);
                statement.setInt(2, to);
                ResultSet resultSet = statement.executeQuery();

                loadMultipleStudentsFromResultSet(studentList, resultSet);
            }
        }
        return studentList;
    }

    private void loadMultipleStudentsFromResultSet(List<Student> studentList, ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            Student student = loadStudentFromResultSet(resultSet);

            studentList.add(student);
        }
    }

    private Student loadStudentFromResultSet(ResultSet resultSet) throws SQLException {
        Student student = new Student();

        student.setId(resultSet.getLong(1));
        student.setName(resultSet.getString(2));
        student.setAge(resultSet.getInt(3));
        student.setAverage(resultSet.getDouble(4));
        student.setAlive(resultSet.getBoolean(5));
        return student;
    }
}
